/*
Navicat MySQL Data Transfer

Source Server         : Mysql Local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : cv

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-03-16 14:04:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `carnet_conducir`
-- ----------------------------
DROP TABLE IF EXISTS `carnet_conducir`;
CREATE TABLE `carnet_conducir` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(200) DEFAULT NULL,
  `creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of carnet_conducir
-- ----------------------------
INSERT INTO `carnet_conducir` VALUES ('1', 'No Posee', '2020-03-06 18:59:13', null);

-- ----------------------------
-- Table structure for `ci_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`,`ip_address`),
  KEY `ci_sessions_timestamp` (`timestamp`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('10lj76k5p86r56e2su9lj6g6g1rg0t1j', '::1', '1583609230', '__ci_last_regenerate|i:1583609230;');
INSERT INTO `ci_sessions` VALUES ('13pl3i1pv6hts9jn40t9gnv6beadfesi', '::1', '1583590923', '__ci_last_regenerate|i:1583590923;');
INSERT INTO `ci_sessions` VALUES ('1e61t5937i15q0mnlue1b72e68m0b909', '::1', '1583519370', '__ci_last_regenerate|i:1583519370;');
INSERT INTO `ci_sessions` VALUES ('1p38hp061r8retnk3uf7h8sq529t3g0a', '::1', '1583525630', '__ci_last_regenerate|i:1583525630;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('1qsdl17n0k322i5bhl7ocing05grmlu8', '::1', '1583520105', '__ci_last_regenerate|i:1583520105;');
INSERT INTO `ci_sessions` VALUES ('22bcrtarlkb7llp59njg1bp1tfkebit9', '::1', '1584379402', '__ci_last_regenerate|i:1584379402;');
INSERT INTO `ci_sessions` VALUES ('23nor6i1violr1mtavjuufaf39b4gju9', '::1', '1583609230', '__ci_last_regenerate|i:1583609230;');
INSERT INTO `ci_sessions` VALUES ('2489prlk8jlhaievjpo2qrfg698btbkg', '::1', '1584366579', '__ci_last_regenerate|i:1584366579;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('2itn14qblbr4n6j7t9osfue8su5prqc5', '127.0.0.1', '1584372187', '__ci_last_regenerate|i:1584372187;');
INSERT INTO `ci_sessions` VALUES ('2rl8jpdb93d54c85ncda2c7epd9krg1e', '::1', '1584374639', '__ci_last_regenerate|i:1584374639;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('3fdfdl10t4lo3600gj9dabs45nun8eu5', '::1', '1583581889', '__ci_last_regenerate|i:1583581889;');
INSERT INTO `ci_sessions` VALUES ('3ifu5lrc4cpnj6ms4nnmrfn36u5tten3', '::1', '1583594459', '__ci_last_regenerate|i:1583594459;');
INSERT INTO `ci_sessions` VALUES ('3jc0jdncrtu2r0curvori3pn7237sein', '::1', '1584369347', '__ci_last_regenerate|i:1584369347;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('3lp38tshbcrcbircoc11icgghl1f19md', '::1', '1583536066', '__ci_last_regenerate|i:1583536066;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('432vsdas6q4psqgdqfhjuqdg7l3oh4eu', '::1', '1583592766', '__ci_last_regenerate|i:1583592766;');
INSERT INTO `ci_sessions` VALUES ('46460m86fjqvo8q1drq1ck3av0976hq7', '::1', '1583504112', '__ci_last_regenerate|i:1583504112;');
INSERT INTO `ci_sessions` VALUES ('49on0t8t75l0u9ttremprq7gi420nuc0', '::1', '1583594110', '__ci_last_regenerate|i:1583594110;');
INSERT INTO `ci_sessions` VALUES ('4dq8e34acu9pbvgdvo5akc986dhusoai', '::1', '1583594632', '__ci_last_regenerate|i:1583594459;');
INSERT INTO `ci_sessions` VALUES ('4u16taqahlafql8i5p8oa0qcg1v4ngav', '::1', '1583513751', '__ci_last_regenerate|i:1583513751;');
INSERT INTO `ci_sessions` VALUES ('4ubbcfdn3u3snvebon4tc92edm24el4e', '::1', '1584381447', '__ci_last_regenerate|i:1584381447;id|s:8:\"14441556\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('58m3vuntuperndo71et28u50d9hc3b24', '127.0.0.1', '1584372453', '__ci_last_regenerate|i:1584372453;');
INSERT INTO `ci_sessions` VALUES ('5eu2jm8r53amstfp8sac5l31r1mofkhi', '127.0.0.1', '1584372476', '__ci_last_regenerate|i:1584372476;');
INSERT INTO `ci_sessions` VALUES ('5llgupknhjdtg3lrljt79m5d8jcgbhf1', '::1', '1584367130', '__ci_last_regenerate|i:1584367130;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('60i7uqcvijv7vkch36nvuju67m411s25', '::1', '1583505095', '__ci_last_regenerate|i:1583505095;');
INSERT INTO `ci_sessions` VALUES ('68g4o3ulukkfhe514ubd34e547vg906h', '::1', '1583519035', '__ci_last_regenerate|i:1583519035;');
INSERT INTO `ci_sessions` VALUES ('6bfg67mqkv0umactluj8k77f613t7nhi', '::1', '1583530247', '__ci_last_regenerate|i:1583530247;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('6gj5tgeglot2uug41r47rvkt4v90jih2', '::1', '1583521032', '__ci_last_regenerate|i:1583521032;id|s:1:\"4\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:3:\"Rod\";role|s:7:\"Cliente\";');
INSERT INTO `ci_sessions` VALUES ('77u57g94kgm42rnrp26chj95qt2qiqkl', '::1', '1583503690', '__ci_last_regenerate|i:1583503690;');
INSERT INTO `ci_sessions` VALUES ('77u7881j326g1cfvonmp98r4fpkrq242', '::1', '1583592058', '__ci_last_regenerate|i:1583592058;');
INSERT INTO `ci_sessions` VALUES ('78o7bm02r3teecrl3jm8srgns741jucn', '::1', '1583590216', '__ci_last_regenerate|i:1583590216;');
INSERT INTO `ci_sessions` VALUES ('78v4thobm00nq4cn10eb8bns8fl7ia6k', '::1', '1583520712', '__ci_last_regenerate|i:1583520712;id|s:1:\"4\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:3:\"Rod\";role|s:7:\"Cliente\";');
INSERT INTO `ci_sessions` VALUES ('7m71uv43nj0ci3g8e6l3tb24he8ar3vq', '::1', '1583527402', '__ci_last_regenerate|i:1583527402;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('7t6shv06hc037n6fmo6kj72emb9h3euh', '::1', '1583532986', '__ci_last_regenerate|i:1583532986;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('85tot2381fhcka96q5b5j8isget8r4k0', '::1', '1583580224', '__ci_last_regenerate|i:1583580224;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('9oahkhc8joi6m6u2ufjsn9v5jietjod3', '::1', '1584372453', '__ci_last_regenerate|i:1584372453;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('9v9ene332lr1l6tr0ncupsf8suq207fr', '::1', '1583408106', '__ci_last_regenerate|i:1583408106;');
INSERT INTO `ci_sessions` VALUES ('adra2ks543havmtmtmt9oupb91pr00rn', '::1', '1583534982', '__ci_last_regenerate|i:1583534982;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('afnlhj83jph5l28rh9da0uvoq6ngfjdu', '::1', '1584373854', '__ci_last_regenerate|i:1584373854;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('ahhk0fpmuu5trmh5rbm4tkifquuctb3e', '::1', '1583509107', '__ci_last_regenerate|i:1583509107;');
INSERT INTO `ci_sessions` VALUES ('alsuvjtcf5jdls332354od8svm987q38', '::1', '1583529295', '__ci_last_regenerate|i:1583529295;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('bfhnh7flh5fkdrojt5gppvv1va0t898d', '::1', '1583521348', '__ci_last_regenerate|i:1583521348;id|s:1:\"4\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:3:\"Rod\";role|s:7:\"Cliente\";');
INSERT INTO `ci_sessions` VALUES ('biafo0ni5snivd4toh0gf807b3lphou5', '::1', '1584374212', '__ci_last_regenerate|i:1584374212;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('bjavl70tsbe0vc3rpocdjpaf5j8dn6jm', '::1', '1583538123', '__ci_last_regenerate|i:1583538123;');
INSERT INTO `ci_sessions` VALUES ('bsntu4cg6fudpskl5jjc77nlod99hkk1', '::1', '1583524186', '__ci_last_regenerate|i:1583524186;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('buhs38ti3togvmbf0dsgn29ko749jp3b', '::1', '1583522313', '__ci_last_regenerate|i:1583522313;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('c6hatmdtabrugt6ig17a3qfrtuu48khd', '::1', '1583407584', '__ci_last_regenerate|i:1583407584;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:6:\"Jeison\";role|s:13:\"Administrador\";');
INSERT INTO `ci_sessions` VALUES ('c8eumeetnq8lt2fkfnaltrf958dh09d9', '127.0.0.1', '1584372911', '__ci_last_regenerate|i:1584372911;');
INSERT INTO `ci_sessions` VALUES ('cdhb2ogjcdoasblqhjhc6uu9sbbsjocf', '127.0.0.1', '1584372679', '__ci_last_regenerate|i:1584372679;');
INSERT INTO `ci_sessions` VALUES ('ce3nra8j986e1iofauq8geqrrve8ol1h', '::1', '1583511322', '__ci_last_regenerate|i:1583511322;');
INSERT INTO `ci_sessions` VALUES ('cffvntfeov6qn430bo5s4sto0sarie4u', '::1', '1583526614', '__ci_last_regenerate|i:1583526614;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('ct0dm2mclrin641cqldnvak8plinfg7m', '::1', '1583590615', '__ci_last_regenerate|i:1583590615;');
INSERT INTO `ci_sessions` VALUES ('d5kcin0hqnd7p7f4crofl168vaht95cn', '::1', '1583608882', '__ci_last_regenerate|i:1583608882;');
INSERT INTO `ci_sessions` VALUES ('dhmda6h1o1uvo3btfbmbln01of21svgk', '::1', '1583608510', '__ci_last_regenerate|i:1583608510;');
INSERT INTO `ci_sessions` VALUES ('dm42q660jufb7cdgm6ads55kjj25ad8u', '::1', '1583523787', '__ci_last_regenerate|i:1583523787;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('dubd0ltpfm4r6b9039ahgenlti1uln7g', '::1', '1583415084', '__ci_last_regenerate|i:1583415084;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:6:\"Jeison\";role|s:13:\"Administrador\";');
INSERT INTO `ci_sessions` VALUES ('egnil6oqk6dhvnblr4kr72hrp2bau6e7', '::1', '1583582265', '__ci_last_regenerate|i:1583582265;');
INSERT INTO `ci_sessions` VALUES ('eqj50u0ddd9skfpk681kko5aq0gthes8', '::1', '1583504663', '__ci_last_regenerate|i:1583504663;');
INSERT INTO `ci_sessions` VALUES ('fl8gl04ul21flge4dqqfloghllpd5v36', '::1', '1583416536', '__ci_last_regenerate|i:1583416536;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:13:\"Administrador\";');
INSERT INTO `ci_sessions` VALUES ('g8jufctml08osijetpl1a91jjdkhjkvs', '::1', '1583538123', '__ci_last_regenerate|i:1583538123;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('g9craggfo5f0qc9oaf1p4ff2s75odbdu', '::1', '1584372863', '__ci_last_regenerate|i:1584372863;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('gcdgjd24a8idut3u2ag025blfjmcn0f9', '::1', '1583509737', '__ci_last_regenerate|i:1583509737;');
INSERT INTO `ci_sessions` VALUES ('gn1itrj1g02jntfnnc8ah8mh00kf0s2f', '::1', '1583593745', '__ci_last_regenerate|i:1583593745;');
INSERT INTO `ci_sessions` VALUES ('h80m0h3m6ms7m0b5445kkq8m8q4oo2jn', '::1', '1583503299', '__ci_last_regenerate|i:1583503299;');
INSERT INTO `ci_sessions` VALUES ('h9anck6pu6psmououjtvm8p8f55hviit', '127.0.0.1', '1584372679', '__ci_last_regenerate|i:1584372679;');
INSERT INTO `ci_sessions` VALUES ('hh6l33tl1qlbu8fu14hg8ol4ebh7ald6', '::1', '1583514978', '__ci_last_regenerate|i:1583514978;');
INSERT INTO `ci_sessions` VALUES ('hqenp2remh9ts8ddqgtlb7aq2h2ftjqd', '::1', '1583406985', '__ci_last_regenerate|i:1583406985;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:6:\"Jeison\";role|s:13:\"Administrador\";');
INSERT INTO `ci_sessions` VALUES ('i4p12vskl2m8ehu8dr0rftecf76988av', '::1', '1583505443', '__ci_last_regenerate|i:1583505443;');
INSERT INTO `ci_sessions` VALUES ('iai9ae5g4qr6rashto964g7knqinmd3d', '::1', '1583530768', '__ci_last_regenerate|i:1583530768;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('iam5r3qcoakf84qctjm8d9pi86otbign', '::1', '1583511724', '__ci_last_regenerate|i:1583511724;');
INSERT INTO `ci_sessions` VALUES ('iatt3bcikna36ncqivs13vc4ss2rcls8', '::1', '1584368668', '__ci_last_regenerate|i:1584368668;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('iibpgi22nb9lhumeud8kihvb5ek3r9lh', '::1', '1584371226', '__ci_last_regenerate|i:1584371226;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('iq7cfrof961nv3qkqg617rqi58tastnd', '::1', '1584367725', '__ci_last_regenerate|i:1584367725;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('jfgo1ojejigu06bc5omqi1369s4eh9gv', '::1', '1583406638', '__ci_last_regenerate|i:1583406638;');
INSERT INTO `ci_sessions` VALUES ('jg4pdscs6uhtm4uqdvj7rf6k95dc6eko', '::1', '1583519773', '__ci_last_regenerate|i:1583519773;');
INSERT INTO `ci_sessions` VALUES ('jkuaat4ub35odp29rsjbqrc5sqqvhals', '::1', '1583591356', '__ci_last_regenerate|i:1583591356;');
INSERT INTO `ci_sessions` VALUES ('k1amegasseqgmknat0baffg5atk44ltk', '::1', '1583416787', '__ci_last_regenerate|i:1583416593;');
INSERT INTO `ci_sessions` VALUES ('kfhp1q7rv4l4nu12vg0ovgc9ecqr0igk', '::1', '1584374970', '__ci_last_regenerate|i:1584374970;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('kreot7cir3i2ier3mk3chjub6pbnt715', '::1', '1583583050', '__ci_last_regenerate|i:1583583050;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('krrovq6ns9faba6ispk68nbb61vcap0n', '::1', '1583537422', '__ci_last_regenerate|i:1583537422;');
INSERT INTO `ci_sessions` VALUES ('kt0i5k123l7he0r2dj78arnso7qb7qjn', '127.0.0.1', '1584372749', '__ci_last_regenerate|i:1584372749;');
INSERT INTO `ci_sessions` VALUES ('l0e5t23pacsjvf1p1lhqfurgc24v0vrj', '::1', '1583520411', '__ci_last_regenerate|i:1583520411;id|s:1:\"4\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:3:\"Rod\";role|s:7:\"Cliente\";');
INSERT INTO `ci_sessions` VALUES ('m4ea4sts14vjbjpppg5tqe9kb714gsga', '::1', '1583523077', '__ci_last_regenerate|i:1583523077;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('mg626ml603ac2a2v4pu4rqhvl6bea8jj', '::1', '1583408416', '__ci_last_regenerate|i:1583408416;');
INSERT INTO `ci_sessions` VALUES ('moh6gnohhgk3m4c64th73cqdmvepfmhe', '127.0.0.1', '1584372187', '__ci_last_regenerate|i:1584372187;');
INSERT INTO `ci_sessions` VALUES ('ms03r79n7aaqbq9cauoi2p8g2944f11n', '::1', '1584373266', '__ci_last_regenerate|i:1584373266;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('n4do9okbcs9i48l0q7qe8a30r7fr0ost', '::1', '1583526282', '__ci_last_regenerate|i:1583526282;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('n7q0kse1gdrgoot27irls2anovo0qsd4', '127.0.0.1', '1584372911', '__ci_last_regenerate|i:1584372911;');
INSERT INTO `ci_sessions` VALUES ('ominivoenh6vdlhclhuk9l98u1boqukh', '::1', '1583534085', '__ci_last_regenerate|i:1583534085;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('opavftgpqjl7rlvjeo4nidcrhjd3u8hc', '::1', '1583535666', '__ci_last_regenerate|i:1583535666;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('pacvg6ooomi1j7n5tpmj9r6f4k3no0is', '::1', '1583523445', '__ci_last_regenerate|i:1583523445;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('q7htfnnfntocl83sh9fjnvf4o0r11qbc', '127.0.0.1', '1584372729', '__ci_last_regenerate|i:1584372729;');
INSERT INTO `ci_sessions` VALUES ('s1fi43tsn21cos545iu7tectiohbgjts', '::1', '1583505873', '__ci_last_regenerate|i:1583505873;');
INSERT INTO `ci_sessions` VALUES ('sljbur80qi4pjnbkahappa6eb8ib7tst', '127.0.0.1', '1584372749', '__ci_last_regenerate|i:1584372749;');
INSERT INTO `ci_sessions` VALUES ('slpqkqh18clih514oem3saceigquffjc', '::1', '1583534537', '__ci_last_regenerate|i:1583534537;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('sqs3qtg9kdrethi6h4ncr5u0d3dk3pph', '::1', '1584375886', '__ci_last_regenerate|i:1584375886;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('torp11rh02pndec1h2m3u8semn2opjt6', '127.0.0.1', '1584372453', '__ci_last_regenerate|i:1584372453;');
INSERT INTO `ci_sessions` VALUES ('u5dgaa1evbl5jrm1pn9on3lktqtd1scn', '::1', '1584368079', '__ci_last_regenerate|i:1584368079;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('ufvvljge52b5pen6s1b957gujmkmi3rt', '127.0.0.1', '1584372476', '__ci_last_regenerate|i:1584372476;');
INSERT INTO `ci_sessions` VALUES ('ul9im26m2k01ctam5a3vc0qobusl2755', '::1', '1584381699', '__ci_last_regenerate|i:1584381699;');
INSERT INTO `ci_sessions` VALUES ('umkaa1q16885bkuvkougghitnm84spda', '::1', '1583525965', '__ci_last_regenerate|i:1583525965;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:15:\"Patricio Malaga\";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('uro3hbp9cr72uiqvc3619887bnrlai4f', '::1', '1584379723', '__ci_last_regenerate|i:1584379723;id|s:8:\"14441556\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');
INSERT INTO `ci_sessions` VALUES ('vquh4qijos0tkm7k35odihjudrsdp7kl', '127.0.0.1', '1584372729', '__ci_last_regenerate|i:1584372729;');
INSERT INTO `ci_sessions` VALUES ('vvg9pparnreda7pal67uoileke868t5a', '::1', '1584371919', '__ci_last_regenerate|i:1584371919;id|s:1:\"2\";login|s:23:\"rodriguezrod1@gmail.com\";names|s:9:\"Patricio \";role|s:7:\"Cliente\";foto|s:44:\"uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG\";');

-- ----------------------------
-- Table structure for `direcciones_pringles`
-- ----------------------------
DROP TABLE IF EXISTS `direcciones_pringles`;
CREATE TABLE `direcciones_pringles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` mediumtext COMMENT 'Direcciones en Coronel Pringles',
  `creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of direcciones_pringles
-- ----------------------------
INSERT INTO `direcciones_pringles` VALUES ('1', 'Cualquiera', '2020-03-06 09:51:58', null);

-- ----------------------------
-- Table structure for `resumen_curricular`
-- ----------------------------
DROP TABLE IF EXISTS `resumen_curricular`;
CREATE TABLE `resumen_curricular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `formacion` enum('Primario Incompleto','Primario Completo','Secundario Incompleto','Secundario Completo','Terciario Incompleto','Terciario Completo','Universitario Incompleto','Universitario Completo') DEFAULT NULL,
  `observaciones_f` text,
  `cursos` text,
  `disponibilidad` enum('Full-Time','Part-Time (Mañana)','Part-Time (Tarde)','Part-Time (Noche)','Sólo fines de semana') DEFAULT NULL,
  `idiomas` mediumtext,
  `conducir_id` int(11) DEFAULT NULL,
  `word` enum('Bajo','Medio','Avanzado') DEFAULT NULL,
  `observaciones_1` varchar(200) DEFAULT NULL,
  `excel` enum('Bajo','Medio','Avanzado') DEFAULT NULL,
  `observaciones_2` varchar(200) DEFAULT NULL,
  `internet` enum('Bajo','Medio','Avanzado') DEFAULT NULL,
  `observaciones_3` varchar(200) DEFAULT NULL,
  `referencias` text,
  `trab_anterior` tinyint(1) DEFAULT NULL COMMENT 'Trabajo anteriormente en el municipio ?',
  `observaciones_g` text,
  `compartir` tinyint(1) DEFAULT NULL COMMENT 'compartir resumen curricular ?',
  `creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `conducir_id` (`conducir_id`),
  CONSTRAINT `resumen_curricular_ibfk_2` FOREIGN KEY (`conducir_id`) REFERENCES `carnet_conducir` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `resumen_curricular_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of resumen_curricular
-- ----------------------------
INSERT INTO `resumen_curricular` VALUES ('2', '14441556', 'Universitario Completo', 'Lcdo. Informática', 'Programación Web', 'Part-Time (Mañana)', 'Español', '1', 'Avanzado', 'gfdgfdgd', 'Avanzado', 'dfgdfgdfg', 'Avanzado', 'dfgfdgd', 'Ninguna de momentos.', '1', 'Las misma de siempre. ', '1', '2020-03-06 19:06:28', null);

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(50) NOT NULL,
  `tipo_id` enum('Administrador','Cliente') COLLATE utf8_unicode_ci DEFAULT 'Cliente',
  `login` varchar(188) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` mediumtext COLLATE utf8_unicode_ci,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `genero` enum('Masculino','Femenino') CHARACTER SET utf8 DEFAULT NULL,
  `reside` tinyint(1) DEFAULT NULL COMMENT 'Reside en Coronel Pringles (SI/NO)',
  `dni` int(50) NOT NULL,
  `direccion_reside_id` int(11) DEFAULT NULL COMMENT 'Id de Direccion donde reside en caso de ser si',
  `provincia` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localidad` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `calle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `altura` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `piso` int(2) DEFAULT NULL,
  `nro_depto` int(3) DEFAULT NULL,
  `telef_personal` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telef_secundario` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_req_passreset` datetime DEFAULT NULL,
  `passreset_code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `actualizado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tipo_id` (`tipo_id`) USING BTREE,
  KEY `direccion_reside_id` (`direccion_reside_id`),
  KEY `dni` (`dni`),
  KEY `id` (`id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`direccion_reside_id`) REFERENCES `direcciones_pringles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('14441556', 'Cliente', 'rodriguezrod1@gmail.com', '$2a$08$gUHGhms3TFAXSer6uknOZ.6kqbEbC8dJSmN6ZAqsMqGEQB2u2f5i.', 'uploads/5baf75a2a8d3bf2eeb81272a1e7fdbc7.JPG', 'Patricio ', 'Malaga', 'Masculino', null, '14441556', '1', null, null, null, null, null, null, null, null, '2020-03-16 14:27:55', '::1', null, null, '1', '2018-02-28 15:40:49', '2019-07-20 12:48:00');
