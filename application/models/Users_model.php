<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Users_model extends CI_Model
{


    //    Consulta las tipos de carnet de conducir
    public function getTiposCarnet()
    {
        return $this->db->get('carnet_conducir')->result();
    }

    //    Consulta las direcciones Coronel Pringlez
    public function getDireccionesPringles()
    {
        return $this->db->get('direcciones_pringles')->result();
    }


    //    Registro de Datos del curriculo 
    public function setRegistroCv()
    {
        try {
            //Iniciamos la transacción.
            $this->db->trans_begin();

            $data = array();

            foreach ($this->input->post() as $key => $value) {
                $data[$key] = $value;
            }

            unset($data['editar']);

            switch ($this->input->post('editar')) {
                case 1:
                    $this->db->where('user_id', $data['user_id'])
                        ->update('resumen_curricular', $data);
                    break;
                case 0:
                    $this->db->insert('resumen_curricular', $data);
                    break;
                default:
                    # code...
                    break;
            }
           // print_r($this->db->last_query()); exit;

            if ($this->db->affected_rows() === 0) {
                throw new Exception('No se Procesaron los datos');
            }

            $this->db->trans_commit();

            return [
                'status'  => 'success',
                'message' => 'Procesado con Exito',
                'code'    => 0
            ];
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
                'code'    => 0
            ];
        }
    }


    //    Consulta los datos del Cliente
    public function getDatosPersonales()
    {
        $this->db->select('u.*, r.*, d.direccion, c.tipo, c.id as id_carnet');
        $this->db->from('users u');
        $this->db->join('direcciones_pringles d', 'd.id=u.direccion_reside_id', 'left');
        $this->db->join('resumen_curricular r', 'u.id=r.user_id', 'left');
        $this->db->join('carnet_conducir c', 'c.id=r.conducir_id', 'left');
        $this->db->where("u.id", $this->session->id);
        return $this->db->get()->row();
    }


    //    Login de ingreso al sistema
    public function signIn()
    {
        try {
            $res = $this->db->select('*')
                ->from('users u')
                ->where('dni', $this->input->post('login', true))
                ->get()
                ->row();
            if (empty($res)) {
                throw new Exception('Usuario y / o Clave invalida');
            } else {
                $this->load->helper('phpass');
                $hasher = new PasswordHash(8, FALSE);
                if (!$hasher->CheckPassword($this->input->post('password'), $res->password)) {
                    throw new Exception('Usuario y / o Clave invalida');
                }
            }

            if ($res->is_active == 0) {
                throw new Exception('Este usuario esta desactivado, Contacte a su administrador');
            }

            $this->db->where('id', $res->id)
                ->update('users', [
                    'last_login' => date('Y-m-d H:i:s', time()),
                    'last_ip'    => $_SERVER['REMOTE_ADDR']
                ]);

            $userData = [
                'id'        => $res->id,
                'login'     => $res->login,
                'names'     => $res->name,
                'role'      => $res->tipo_id,
                'foto'      => $res->photo
            ];
            $this->session->set_userdata($userData);

            return [
                'status'  => 'success',
                'message' => 'Bienvenido al Sistema',
                'code'    => 0
            ];
        } catch (Exception $e) {
            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
                'code'    => 0
            ];
        }
    }

    //    Registro de Nuevos Usuarios
    public function register()
    {
        try {
            //Iniciamos la transacción.
            $this->db->trans_begin();

            $this->load->helper('phpass');
            $hasher = new PasswordHash(8, FALSE);

            //Configuramos los parametros para subir el archivo al servidor.
            $config['upload_path'] = 'uploads';
            $config['allowed_types'] = "gif|jpg|png|jpeg|";
            $config['encrypt_name'] = true;
            $config['max_size'] = '10000';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('foto')) {
                throw new Exception('Hay un error: ' . $this->upload->display_errors());
            }

            $datos = array('foto' => $this->upload->data());
            $archivo = $config['upload_path'] . '/' . $datos['foto']['file_name'];

            chmod($archivo, 0777);

            $this->db->insert('users', [
                'id'    => $this->input->post('dni', true),
                'name'    => $this->input->post('nombre', true),
                'last_name'       => $this->input->post('apellido', true),
                'dni'      => $this->input->post('dni', true),
                'photo'      => $archivo,
                'reside'      => $this->input->post('recide', true),
                'direccion_reside_id'      => $this->input->post('direccion_recide', true),
                'provincia'      => $this->input->post('provincia', true),
                'localidad'      => $this->input->post('localidad', true),
                'calle'      => $this->input->post('calle', true),
                'altura'      => $this->input->post('altura', true),
                'piso'      => $this->input->post('piso', true),
                'nro_depto'      => $this->input->post('departamento', true),
                'telef_personal'      => $this->input->post('telefono_1', true),
                'telef_secundario'      => $this->input->post('telefono_2', true),
                'genero'      => $this->input->post('genero', true),
                'login'      => $this->input->post('correo', true),
                'password'   => $hasher->HashPassword($this->input->post('password')),
                'created_at' => date('Y-m-d H:i:s', time())
            ]);

            if ($this->db->affected_rows() === 0) {
                throw new Exception('No se Registraron los datos');
            }

            $res = $this->db->select('*')
                ->from('users u')
                ->where('dni', $this->input->post('dni', true))
                ->get()
                ->row();

            $userData = [
                'id'        => $res->id,
                'login'     => $res->login,
                'names'     => $res->name,
                'role'      => $res->tipo_id,
                'foto'      => $res->photo
            ];
            $this->session->set_userdata($userData);

            $this->db->trans_commit();

            return [
                'status'  => 'success',
                'message' => 'Registrado con Exito',
                'code'    => 0
            ];
        } catch (Exception $e) {
            $this->db->trans_rollback();
            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
                'code'    => 0
            ];
        }
    }

    //    Verifica el token para poder cambiar la contraseña 
    public function verifyCode($token, $id)
    {
        $result = $this->db->select('passreset_code')
            ->from('users')
            ->where('id', $id)
            ->get()
            ->row();
        return $result->passreset_code != $token ? false : true;
    }

    // Funcion inicializadora de envios de email 
    public function sendEmail($to, $message)
    {
        $this->email->initialize();
        $this->email->from('modernizacion@coronelpringles.gov.ar', 'CV Pringles');
        $this->email->to($to);
        //        $this->email->cc('another@another-example.com');
        //        $this->email->bcc('them@their-example.com');
        $this->email->subject('¡Importante!');
        $this->email->message($message);
        return $this->email->send();
    }


    //    Consulta el email y manda el correo con el link del token de cambio de contraseña
    public function resetPassword()
    {
        try {
            $res = $this->db->select('id, name, last_name')
                ->from('users')
                ->where('login', $this->input->post('email_recover', true))
                ->get()
                ->row();

            if (empty($res)) {
                throw new Exception('Email no existe ...');
            }

            $uniqidStr     = md5(uniqid(mt_rand()));
            $resetPassLink = base_url() . 'changePassword/' . $uniqidStr . '/' . $res->id;

            $body = "Hola!!!,  <b>" . $res->name . " " . $res->last_name . "</b>, Por favor dale click <a href='" . $resetPassLink . "'>AQUI</a> para cambiar la clave de acceso .";

            if (!$this->sendEmail($this->input->post('email_recover'), $body)) {
                throw new Exception('Email no enviado: ' . $this->email->print_debugger());
            }

            $this->db->where('id', $res->id)
                ->update('users', ['passreset_code' => $uniqidStr]);

            return [
                'status'  => 'success',
                'message' => 'Por favor, compruebe su correo electrónico suministrado que allí tiene las instrucciones...',
                'code'    => 0
            ];
        } catch (Exception $e) {
            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
                'code'    => 0
            ];
        }
    }

    //    Cambia la contraseña una vez verificado el token
    public function recoverPassword()
    {
        try {
            $this->load->helper('phpass');
            $hasher = new PasswordHash(8, FALSE);

            $this->db->where('id', $this->input->post('id', true))
                ->update('users', ['password' => $hasher->HashPassword($this->input->post('password'))]);

            if ($this->db->affected_rows() === 0) {
                throw new Exception('Hay un error en el proceso ...');
            }
            return [
                'status'  => 'success',
                'message' => 'Su contraseña ha sido restablecida con éxito',
                'code'    => 0
            ];
        } catch (Exception $e) {
            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
                'code'    => 0
            ];
        }
    }

    //    Cambia la contraseña una vez que este login
    public function changePassword()
    {
        try {
            $this->load->helper('phpass');
            $hasher = new PasswordHash(8, FALSE);

            $this->db->where('id', $this->session->id)
                ->update('users', ['password' => $hasher->HashPassword($this->input->post('password_repeat'))]);
            if ($this->db->affected_rows() === 0) {
                throw new Exception('Hay un error en el proceso...');
            }
            return [
                'status'  => 'success',
                'message' => 'Su contraseña ha sido restablecida con éxito',
                'code'    => 0
            ];
        } catch (Exception $e) {
            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
                'code'    => 0
            ];
        }
    }

    public function getBySiteId()
    {
        $result = $this->db->select('id, is_active, login, site_id, role')
            ->where('site_id', $this->input->post('id'))
            ->get('users')
            ->result();

        return [
            'status'  => 'success',
            'message' => '',
            'code'    => 0,
            'data'    => $result
        ];
    }

    //    Verifica si el pass esta correcto comparando el ingresado con el guardado en el sistema
    public function isCurrentPassword()
    {
        $this->load->helper('phpass');
        $hasher = new PasswordHash(8, FALSE);

        $res = $this->db->select('password')
            ->from('users')
            ->where([
                'id' => $this->session->id
            ])
            ->get()
            ->row();
        return $hasher->CheckPassword($this->input->post('current_password'), $res->password);
    }

    //    Verifica si el login o DNI es unico cuando se van a crear nuevos usuarios
    public function isUniqueLogin()
    {
        $where['dni'] = $this->input->post('dni');
        if (!empty($this->input->post('id'))) {
            $where['id !='] = $this->input->post('id');
        }
        $numRows = $this->db->select('id')
            ->from('users')
            ->where($where)
            ->get()
            ->num_rows();

        // si esta en sesión y es el mismo login lo pasa
        //if ($this->session->login == $where['login']) {
        //  return true;
        // }
        return $numRows === 0 ? true : false;
    }

    //    Verifica si el login o email es unico cuando se van a crear nuevos usuarios
    public function isUniqueLoginEmail()
    {
        $where['login'] = $this->input->post('correo');
        if (!empty($this->input->post('id'))) {
            $where['id !='] = $this->input->post('id');
        }
        $numRows = $this->db->select('id')
            ->from('users')
            ->where($where)
            ->get()
            ->num_rows();
        return $numRows === 0 ? true : false;
    }

    //    Borrar registros desde cualquier tabla 
    public function delete()
    {
        try {
            $tabla = $this->input->post('tabla', true);
            $id    = $this->input->post('id', true);

            $this->db->where('id', $id)->delete($tabla);

            if ($this->db->affected_rows() === 0) {
                throw new Exception('Hubo un problema al intentar eliminar este registro.');
            }
            return [
                'status'  => 'success',
                'message' => 'Registro eliminado exitosamente',
                'code'    => 0
            ];
        } catch (Exception $e) {
            return [
                'status'  => 'error',
                'message' => $e->getMessage(),
                'code'    => 0
            ];
        }
    }
}
