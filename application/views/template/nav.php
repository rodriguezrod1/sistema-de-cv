 <!-- ============================================================== -->
 <!-- Left Sidebar - style you can find in sidebar.scss  -->
 <!-- ============================================================== -->
 <div class="side-mini-panel">
     <ul class="mini-nav">
         <div class="togglediv"><a href="javascript:void(0)" id="togglebtn"><i class="ti-menu"></i></a></div>
         <!-- .Dashboard -->
         <li>
             <a href="javascript:void(0)"><i class="icon-speedometer"></i></a>
             <div class="sidebarmenu">
                 <!-- Left navbar-header -->
                 <h3 class="menu-title">Dashboard</h3>
                 <ul class="sidebar-menu">
                     <li><a href="<?= site_url('Home/') ?>">Inicio</a></li>
                 </ul>
                 <!-- Left navbar-header end -->
             </div>
         </li>
    
         <li>
             <a href="javascript:void(0)"><i class="icon-lock"></i></a>
             <div class="sidebarmenu">
                 <!-- Left navbar-header -->
                 <h3 class="menu-title">Cambio Clave</h3>
                 <ul class="sidebar-menu">
                     <li><a href="<?= site_url('password/') ?>">Cambio Clave</a></li>
                 </ul>
                 <!-- Left navbar-header end -->
             </div>
         </li>
         <!-- /.Usuarios -->

     </ul>
 </div>
 <!-- ============================================================== -->
 <!-- End Left Sidebar - style you can find in sidebar.scss  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- Page wrapper  -->
 <!-- ============================================================== -->
 <div class="page-wrapper">
     <!-- ============================================================== -->
     <!-- Container fluid  -->
     <!-- ============================================================== -->
     <div class="container-fluid">
         <!-- ============================================================== -->
         <!-- Bread crumb and right sidebar toggle -->
         <!-- ============================================================== -->
         <div class="row page-titles">
             <div class="col-md-12">
                 <h4 class="text-white"><?= $title ?></h4>
             </div>
             <div class="col-md-6">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="javascript:void(0)">Inicio</a></li>
                     <li class="breadcrumb-item active"><?= $title ?></li>
                 </ol>
             </div>
             <div class="col-md-6 text-right">
                 <!--<a class="btn  btn-outline-info waves-effect " href="https://www.youtube.com/channel/UC98p3ciHmnasoaOUEjdGFYQ?view_as=subscriber&fbclid=IwAR00Se4rQWNtH-GLN7hs37sxfM8MuWm55R89rHuMq4nFlN1NIJPeYnDB4jU" target="_blank" rel="noopener noreferrer">Tutoriales</a>-->
             </div>
         </div>
         <!-- ============================================================== -->
         <!-- End Bread crumb and right sidebar toggle -->
         <!-- ============================================================== -->
         <!-- ============================================================== -->
         <!-- Start Page Content -->
         <!-- ============================================================== -->