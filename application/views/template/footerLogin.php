<input id="base_api" type="hidden" value="<?= base_url(); ?>">
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<script src="<?= base_url(); ?>assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?= base_url(); ?>assets/node_modules/popper/popper.min.js"></script>
<script src="<?= base_url(); ?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/node_modules/toast-master/js/jquery.toast.js"></script>
<!--Custom JavaScript -->
<script src="<?= base_url(); ?>assets/js/funciones.js?v=1a"></script>
<script type="text/javascript">
    $(function() {
        $(".preloader").fadeOut();
    });
    // ============================================================== 
    // Login and Recover Password 
    // ============================================================== 
    $('#to-recover').on("click", function() {
    $("#loginform").slideUp();
    $("#recoverform").fadeIn();
    });
</script>