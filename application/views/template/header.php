</head>

<body class="skin-default fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Coronel Pringles</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= site_url('Home/') ?>">
                        <!-- Logo icon -->
                        <b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="<?= base_url(); ?>assets/images/logo.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img class="img-circle" style="width: 30px; height: 30px" src="<?= base_url(); ?>assets/images/logo-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <li class="d-none d-md-block d-lg-block">
                            <a href="<?= site_url('Home/') ?>" class="p-l-15">
                                Coronel Pringles
                                <!--This is logo text
                                <img src="<?= base_url(); ?>assets/images/logo-light-text.png" alt="home" class="light-logo" alt="home" />-->
                            </a>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="<?= site_url('message/') ?>"> <i class="ti-email"></i>
                                <div id="notificar" class=""> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <?php if ($this->session->role == "Gestor") : ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="<?= site_url('verifications/') ?>" > <i class="icon-note"></i>
                                    <div  id="notificar2" class="">
                                        <span class="heartbit"></span> 
                                        <span class="point"></span>
                                     </div>
                                </a>              
                            </li>
                        <?php endif ?>

                        <?php if ($this->session->role == "Administrador") : ?>
                            <li class="nav-item dropdown">
                                <a  class="nav-link dropdown-toggle waves-effect waves-dark" href="<?= site_url('verifications/') ?>" > <i class="icon-note"></i>
                                    <div  id="notificar3" class="">
                                        <span class="heartbit"></span> 
                                        <span class="point"></span>
                                     </div>
                                </a>              
                            </li>
                        <?php endif ?>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- User Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?= base_url().$this->session->foto ?>" alt="user" class="">
                                <span class="hidden-md-down"><?= $this->session->names . " (" . $this->session->role . ")" ?> &nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <!-- text-->
                                <a href="<?= site_url('password/') ?>" class="dropdown-item"><i class="ti-user"></i> Cambio Clave</a>
                                <div class="dropdown-divider"></div>
                                <!-- text-->
                                <a href="<?= site_url('logout/') ?>" class="dropdown-item"><i class="fa fa-power-off"></i> Salir</a>
                                <!-- text-->
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End User Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item right-side-toggle"> <a class="nav-link  waves-effect waves-light" href="javascript:void(0)"><i class="ti-arrow-right ti-arrow-left"></i></a></li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->