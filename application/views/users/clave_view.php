<?php $this->load->view('template/head', array('title' => $title)); ?>
<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/nav', array('title' => $title)); ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header"><h4>Cambio de Clave</h4></div>
            <div class="card-body">
                <div class="row m-t-20">
                    <div class="col-md-12 ">
                        <form class="floating-labels" id="form_cambio_de_clave">
                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-4">
                                        <input type="password" required class="form-control "  name="current_password" id='current_password' autofocus>
                                        <span class="bar"></span>
                                        <label for="current_password">Clave Anterior</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="password" required class="form-control "  name="password" id='password'>
                                        <span class="bar"></span>
                                        <label for="password">Nueva Clave</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="password" required class="form-control "  name="password_repeat" id='password_repeat'>
                                        <span class="bar"></span>
                                        <label for="password_repeat">Repetir Clave</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <div class="row  m-t-10 justify-content-end">
                                    <div class="col-md-2">
                                        <button type="submit" class="btn btn-outline-primary btn-lg">
                                            <i class="fa fa-save"></i>
                                            <span id="text_contact">Procesar</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('template/footer'); ?>
</body>

</html>