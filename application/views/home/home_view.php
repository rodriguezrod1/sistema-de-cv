<?php $this->load->view('template/head', array('title' => $title)); ?>
<?php $this->load->view('template/header'); ?>
<?php $this->load->view('template/nav', array('title' => $title)); ?>

<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-9">
                        <h4>Ingresa tu Resumen Curricular</h4>
                    </div>
                    <div class="col-md-3">
                        <a target="_blank" href="<?= site_url('pdf/') ?>" type="button" class="btn btn-primary btn-block">
                            <i class="fa fa-print"></i>&nbsp;
                            Imprimir PDF
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <ul class="nav nav-pills m-t-10 m-b-30">
                    <li class=" nav-item"> <a href="#navpills-1" class="nav-link active" data-toggle="tab" aria-expanded="false">Datos Personales</a> </li>
                    <li class=" nav-item"> <a href="#navpills-5" class="nav-link" data-toggle="tab" aria-expanded="false">Cargar otros Datos</a> </li>
                </ul>
                <div class="tab-content br-n pn">

                    <div id="navpills-1" class="tab-pane active">
                        <div class="row">
                            <div class="col-md-12 table-responsive">
                                <h4 class="card-title">Datos Personales</h4>
                                <table style="width: 100%" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Foto</th>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>DNI</th>
                                            <th>Genero</th>
                                            <th>¿Reside Pringles?</th>
                                            <th>Dirección</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><img style="width: 60px; height: 60px" src="<?= base_url() . $this->session->foto ?>" alt="user" class="img-fluid img-circle img-responsive"></td>
                                            <td><?= $data->name ?></td>
                                            <td><?= $data->last_name ?></td>
                                            <td><?= $data->dni ?></td>
                                            <td><?= $data->genero ?></td>
                                            <td><?= $data->reside == 1 ? "Si" : "No"; ?></td>
                                            <td><?= $data->direccion ?></td>
                                        </tr>
                                    </tbody>
                                </table>


                                <table style="width: 100%" class="table table-striped table-bordered">
                                    <thead class="thead-inverse">
                                        <tr>
                                            <th>Provincia</th>
                                            <th>Localidad</th>
                                            <th>Calle</th>
                                            <th>Altura</th>
                                            <th>Piso</th>
                                            <th>Depto</th>
                                            <th>Telf. Personal</th>
                                            <th>Telf. Secundario</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?= $data->provincia != "" ? $data->provincia : "--" ?></td>
                                            <td><?= $data->localidad != "" ? $data->localidad : "--" ?></td>
                                            <td><?= $data->calle != "" ? $data->calle : "--" ?></td>
                                            <td><?= $data->altura != "" ? $data->altura : "--" ?></td>
                                            <td><?= $data->piso != "" ? $data->piso : "--" ?></td>
                                            <td><?= $data->nro_depto != "" ? $data->nro_depto : "--" ?></td>
                                            <td><?= $data->telef_personal != "" ? $data->telef_personal : "--" ?></td>
                                            <td><?= $data->telef_secundario != "" ? $data->telef_secundario : "--" ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <div id="navpills-5" class="tab-pane">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="floating-labels" id="formResumen">

                                    <div class=" form-group m-t-15">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <select autofocus id="formacion" required name="formacion" class="form-control ">
                                                    <option value=""></option>
                                                    <option <?= $data->formacion == "Primario Incompleto" ? "selected" : "" ?> value="Primario Incompleto">Primario Incompleto</option>
                                                    <option <?= $data->formacion == "Primario Completo" ? "selected" : "" ?> value="Primario Completo">Primario Completo</option>
                                                    <option <?= $data->formacion == "Secundario Completo" ? "selected" : "" ?> value="Secundario Completo">Secundario Completo</option>
                                                    <option <?= $data->formacion == "Terciario Incompleto" ? "selected" : "" ?> value="Terciario Incompleto">Terciario Incompleto</option>
                                                    <option <?= $data->formacion == "Terciario Completo" ? "selected" : "" ?> value="Terciario Completo">Terciario Completo</option>
                                                    <option <?= $data->formacion == "Universitario Incompleto" ? "selected" : "" ?> value="Universitario Incompleto">Universitario Incompleto</option>
                                                    <option <?= $data->formacion == "Universitario Completo" ? "selected" : "" ?> value="Universitario Completo">Universitario Completo</option>
                                                </select>
                                                <span class="bar"></span>
                                                <label for="formacion">Formación Academica</label>
                                            </div>
                                            <div class="col-md-4">
                                                <textarea class="form-control " required name="observaciones_f" id="observaciones_f"><?= $data->observaciones_f != "" ? $data->observaciones_f : "" ?></textarea>
                                                <span class="bar"></span>
                                                <label for="observaciones_f">Titulo e Institución</label>
                                            </div>

                                            <div class="col-md-4">
                                                <select id="conducir_id" required name="conducir_id" class="form-control ">
                                                    <option value=""></option>
                                                    <?php foreach ($tipo as $value) : ?>
                                                        <option <?= $data->conducir_id == $value->id ? "selected" : "" ?> value="<?= $value->id ?>"><?= $value->tipo ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                                <span class="bar"></span>
                                                <label for="conducir_id">Carnet de Conducir</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=" form-group m-t-25">
                                        <div class="row">

                                            <div class="col-md-4">
                                                <textarea class="form-control " required name="cursos" id="cursos"><?= $data->cursos != "" ? $data->cursos : "" ?></textarea>
                                                <span class="bar"></span>
                                                <label for="cursos">Cursos Realizados</label>
                                            </div>

                                            <div class="col-md-4">
                                                <select id="disponibilidad" required name="disponibilidad" class="form-control ">
                                                    <option value=""></option>
                                                    <option <?= $data->disponibilidad == "Full-Time" ? "selected" : "" ?> value="Full-Time">Full-Time</option>
                                                    <option <?= $data->disponibilidad == "Part-Time (Mañana)" ? "selected" : "" ?> value="Part-Time (Mañana)">Part-Time (Mañana)</option>
                                                    <option <?= $data->disponibilidad == "Part-Time (Tarde)" ? "selected" : "" ?> value="Part-Time (Tarde)">Part-Time (Tarde)</option>
                                                    <option <?= $data->disponibilidad == "Part-Time (Noche)" ? "selected" : "" ?> value="Part-Time (Noche)">Part-Time (Noche)</option>
                                                    <option <?= $data->disponibilidad == "Sólo fines de semana" ? "selected" : "" ?> value="Sólo fines de semana">Sólo fines de semana</option>
                                                </select>
                                                <span class="bar"></span>
                                                <label for="disponibilidad">Disponibilidad Horaria</label>
                                            </div>

                                            <div class="col-md-4">
                                                <input type="text" value="<?= $data->idiomas != "" ? $data->idiomas : "" ?>" class="form-control" required name="idiomas" id="idiomas">
                                                <span class="bar"></span>
                                                <label for="idiomas">Idiomas</label>
                                            </div>
                                        </div>
                                    </div>

                                    <h4>Conocimientos Ofimáticos</h4>
                                    <hr>

                                    <div class=" form-group m-t-25">
                                        <div class="row">

                                            <div class="col-md-3">
                                                <select id="word" name="word" required class="form-control ">
                                                    <option value=""></option>
                                                    <option <?= $data->word == "Bajo" ? "selected" : "" ?> value="Bajo">Bajo</option>
                                                    <option <?= $data->word == "Medio" ? "selected" : "" ?> value="Medio">Medio</option>
                                                    <option <?= $data->word == "Avanzado" ? "selected" : "" ?> value="Avanzado">Avanzado</option>
                                                </select>
                                                <span class="bar"></span>
                                                <label for="word">Word</label>
                                            </div>

                                            <div class="col-md-3">
                                                <input type="text" value="<?= $data->observaciones_1 != "" ? $data->observaciones_1 : "" ?>" class="form-control" name="observaciones_1" id="observaciones_1">
                                                <span class="bar"></span>
                                                <label for="observaciones_1">Observación</label>
                                            </div>

                                            <div class="col-md-3">
                                                <select id="excel" name="excel" required class="form-control ">
                                                    <option value=""></option>
                                                    <option <?= $data->excel == "Bajo" ? "selected" : "" ?> value="Bajo">Bajo</option>
                                                    <option <?= $data->excel == "Medio" ? "selected" : "" ?> value="Medio">Medio</option>
                                                    <option <?= $data->excel == "Avanzado" ? "selected" : "" ?> value="Avanzado">Avanzado</option>
                                                </select>
                                                <span class="bar"></span>
                                                <label for="excel">Excel</label>
                                            </div>

                                            <div class="col-md-3">
                                                <input type="text" value="<?= $data->observaciones_2 != "" ? $data->observaciones_2 : "" ?>" class="form-control" name="observaciones_2" id="observaciones_2">
                                                <span class="bar"></span>
                                                <label for="observaciones_2">Observación</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=" form-group m-t-25">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <select id="internet" required name="internet" class="form-control ">
                                                    <option value=""></option>
                                                    <option <?= $data->internet == "Bajo" ? "selected" : "" ?> value="Bajo">Bajo</option>
                                                    <option <?= $data->internet == "Medio" ? "selected" : "" ?> value="Medio">Medio</option>
                                                    <option <?= $data->internet == "Avanzado" ? "selected" : "" ?> value="Avanzado">Avanzado</option>
                                                </select>
                                                <span class="bar"></span>
                                                <label for="internet">Internet</label>
                                            </div>

                                            <div class="col-md-3">
                                                <input type="text" value="<?= $data->observaciones_3 != "" ? $data->observaciones_3 : "" ?>" class="form-control" name="observaciones_3" id="observaciones_3">
                                                <span class="bar"></span>
                                                <label for="observaciones_3">Observación</label>
                                            </div>

                                            <div class="col-md-3">
                                                <select id="trab_anterior" required name="trab_anterior" class="form-control ">
                                                    <option value=""></option>
                                                    <option <?= $data->trab_anterior == 1 ? "selected" : "" ?> value="1">Sí</option>
                                                    <option <?= $data->trab_anterior == 0 ? "selected" : "" ?> value="0">No</option>
                                                </select>
                                                <span class="bar"></span>
                                                <label for="trab_anterior">¿Trabajo en el Municipio?</label>
                                            </div>

                                            <div class="col-md-3">
                                                <select id="compartir" required name="compartir" class="form-control ">
                                                    <option value=""></option>
                                                    <option <?= $data->compartir == 1 ? "selected" : "" ?> value="1">Sí</option>
                                                    <option <?= $data->compartir == 0 ? "selected" : "" ?> value="0">No</option>
                                                </select>
                                                <span class="bar"></span>
                                                <label for="compartir">¿Compartir Curriculum?</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=" form-group m-t-25">
                                        <div class="row">

                                            <div class="col-md-6">
                                                <textarea class="form-control " required name="observaciones_g" id="observaciones_g"><?= $data->observaciones_g != "" ? $data->observaciones_g : "" ?></textarea>
                                                <span class="bar"></span>
                                                <label for="observaciones_g">Observaciones</label>
                                            </div>

                                            <div class="col-md-6">
                                                <textarea class="form-control " name="referencias" id="referencias"><?= $data->referencias != "" ? $data->referencias : "" ?></textarea>
                                                <span class="bar"></span>
                                                <label for="referencias">Referencias</label>
                                            </div>

                                        </div>
                                    </div>

                                    <div class=" form-group m-t-25">
                                        <input type="hidden" name="editar" value="<?= $data->formacion != "" ? 1 : 0 ?>">
                                        <input type="hidden" name="user_id" value="<?= $this->session->id ?>">
                                        <div class="row justify-content-end">
                                            <div class="col-md-4">
                                                <button id="text" type="submit" class="btn btn-block btn-primary">
                                                    Procesar
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<?php $this->load->view('template/footer'); ?>
<script src="<?= base_url(); ?>assets/js/home.js?v=2"></script>
</body>

</html>