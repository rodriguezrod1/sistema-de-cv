<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Lcdo. Rod Rodríguez">
    <link rel="icon" type="image/jpeg" sizes="16x16" href="<?= base_url(); ?>assets/images/log_venplus.png">
    <title>Pringles | CV</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
</head>

<body>
    <div class="row">
        <div class="col-md-12">
            <h4 class="card-title">Datos Personales</h4>
            <table style="width: 100%; font-size: 11px" class="table  table-bordered text-center">
                <thead>
                    <tr>                        
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>DNI</th>
                        <th>Genero</th>
                        <th>¿Reside Pringles?</th>
                        <th>Dirección</th>
                        <th>Foto</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>                    
                        <td><?= $data->name ?></td>
                        <td><?= $data->last_name ?></td>
                        <td><?= $data->dni ?></td>
                        <td><?= $data->genero ?></td>
                        <td><?= $data->reside == 1 ? "Si" : "No"; ?></td>
                        <td><?= $data->direccion ?></td>
                        <td><img style="width: 60px; height: 60px" src="<?= base_url() . $this->session->foto ?>" alt="user" class="img-fluid img-circle img-responsive"></td>
                    </tr>
                </tbody>
            </table>


            <table style="width: 100%; font-size: 12px"  class="table  table-bordered text-center">
                <thead>
                    <tr>
                        <th>Provincia</th>
                        <th>Localidad</th>
                        <th>Calle</th>
                        <th>Altura</th>
                        <th>Piso</th>
                        <th>Depto</th>
                        <th>Telf. Personal</th>
                        <th>Telf. Secundario</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $data->provincia != "" ? $data->provincia : "--" ?></td>
                        <td><?= $data->localidad != "" ? $data->localidad : "--" ?></td>
                        <td><?= $data->calle != "" ? $data->calle : "--" ?></td>
                        <td><?= $data->altura != "" ? $data->altura : "--" ?></td>
                        <td><?= $data->piso != "" ? $data->piso : "--" ?></td>
                        <td><?= $data->nro_depto != "" ? $data->nro_depto : "--" ?></td>
                        <td><?= $data->telef_personal != "" ? $data->telef_personal : "--" ?></td>
                        <td><?= $data->telef_secundario != "" ? $data->telef_secundario : "--" ?></td>
                    </tr>
                </tbody>
            </table>

            <hr>
            <h4 class="card-title">Datos Laborales</h4>
            <table style="width: 100%; font-size: 12px"  class="table  table-bordered text-center">
                <thead>
                    <tr>
                        <th>Formación Academica</th>
                        <th>Titulo e Institución</th>
                        <th>Carnet de Conducir</th>                     
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $data->formacion != "" ? $data->formacion : "--" ?></td>
                        <td><?= $data->observaciones_f != "" ? $data->observaciones_f : "--" ?></td>
                        <td><?= $data->tipo != "" ? $data->tipo : "--" ?></td>                      
                    </tr>
                </tbody>
            </table>


            <table style="width: 100%; font-size: 12px"  class="table  table-bordered text-center">
                <thead>
                    <tr>
                        <th>Cursos Realizados</th>
                        <th>Disponibilidad Horaria</th>
                        <th>Idiomas</th>                     
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $data->cursos != "" ? $data->cursos : "--" ?></td>
                        <td><?= $data->disponibilidad != "" ? $data->disponibilidad : "--" ?></td>
                        <td><?= $data->idiomas != "" ? $data->idiomas : "--" ?></td>                      
                    </tr>
                </tbody>
            </table>

            <hr>
            <h4 class="card-title">Conocimientos Ofimáticos</h4>
            <table style="width: 100%; font-size: 12px"  class="table  table-bordered text-center">
                <thead>
                    <tr>
                        <th>Word</th>
                        <th>Observación</th>
                        <th>Excel</th>
                        <th>Observación</th>                     
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $data->word != "" ? $data->word : "--" ?></td>
                        <td><?= $data->observaciones_1 != "" ? $data->observaciones_1 : "--" ?></td>
                        <td><?= $data->excel != "" ? $data->excel : "--" ?></td>            
                        <td><?= $data->observaciones_2 != "" ? $data->observaciones_2 : "--" ?></td>          
                    </tr>
                </tbody>
            </table>


            <table style="width: 100%; font-size: 12px"  class="table  table-bordered text-center">
                <thead>
                    <tr>
                        <th>Internet</th>
                        <th>Observación</th>
                        <th>¿Trabajo en el Municipio?</th>
                        <th>¿Compartir Curriculum?</th>                     
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $data->internet != "" ? $data->internet : "--" ?></td>
                        <td><?= $data->observaciones_3 != "" ? $data->observaciones_3 : "--" ?></td>
                        <td><?= $data->trab_anterior == 1 ? "Sí": "No" ?></td>            
                        <td><?= $data->compartir == 1 ? "Sí": "No" ?></td>          
                    </tr>
                </tbody>
            </table>
            
            <hr>
            <h4 class="card-title">Extras</h4>
            <table style="width: 100%; font-size: 12px"  class="table  table-bordered text-center">
                <thead>
                    <tr>
                        <th>Observaciones</th>
                        <th>Referencias</th>                               
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $data->observaciones_g != "" ? $data->observaciones_g : "--" ?></td>
                        <td><?= $data->referencias != "" ? $data->referencias : "--" ?></td>                     
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>