<?php $this->load->view('template/headLogin'); ?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">CV Coronel Pringles</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div class="row justify-content-center m-t-40">
        <div class="col-md-8">
            <?php if($result == true): ?>
                <section id="wrapper" class="">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal form-material" id="resetPassword">
                                <!-- <div class="text-center">
                            <a href="javascript:void(0)" class="db"><img src="<?= base_url(); ?>assets/images/logo-icon.png" alt="Home" /><br /><img src="<?= base_url(); ?>assets/images/logo-text.png" alt="Home" /></a>
                        </div> -->
                                <h3 class="box-title m-t-20 m-b-0 text-center">Cambie su Contraseña</h3>

                                <div class="form-group m-t-20">
                                    <div class="row justify-content-center">
                                        <div class="col-md-5">
                                            <input autofocus type="password" class="form-control " placeholder="Ingrese Nueva Contraseña" id="password" name="password" required>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="password" class="form-control " placeholder="Repita la Nueva Contraseña" id="password_repeat" name="password_repeat" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group text-center m-t-20 justify-content-end">
                                    <div class="row justify-content-center">
                                        <div class="col-md-4">
                                            <button id="text_recover" class="btn btn-info  btn-block text-uppercase waves-effect waves-light" type="submit">
                                                Cambiar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="id" value="<?= $id ?>">
                            </form>
                        </div>
                    </div>
                </section>
            <?php else: ?>    
                <h1 class="text-danger font-weight-bold text-center">Credenciales NO validas para Cambiar Contraseña...</h1>
                <div class="row justify-content-end">
                    <div class="col-md-2 ">
                        <a href="<?= site_url('/'); ?>" class="btn btn-info btn-block">Login</a>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>

    <?php $this->load->view('template/footerLogin'); ?>
</body>

</html>