<?php $this->load->view('template/headLogin'); ?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">CV Coronel Pringles</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background-image:url('<?= base_url(); ?>assets/images/background/login-register.jpg');">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material text-center m-t-30" id="loginform">
                    <a href="javascript:void(0)" class="db ">
                        <!--   <img class="img-circle" style="width: 40px; height: 40px" src="<?= base_url(); ?>assets/images/logo.png" alt="Home" /><br />
                      <img src="<?= base_url(); ?>assets/images/logo-text.png" alt="Home" />-->
                        <h2>CV Coronel Pringles</h2>
                    </a>
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input class="form-control" type="number" required="" name="login" id="login" placeholder="Usuario">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" id="password" required="" placeholder="Contraseña">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="d-flex no-block align-items-center">
                                <!-- <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                    <label class="custom-control-label" for="customCheck1">Remember me</label>
                                </div> -->
                                <div class="ml-auto">
                                    <a href="javascript:void(0)" id="to-recover" class="text-muted">
                                        <i class="fas fa-lock m-r-5"></i>
                                        ¿ Olvidaste la Contraseña ?
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button id="text_login" class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="submit">Ingresar</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <!-- <div class="social">
                                <button class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fab fa-facebook-f"></i> </button>
                                <button class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fab fa-google-plus-g"></i> </button>
                            </div> -->
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-md-12 text-center">
                            ¿ No estas registrad@ ? <a href="<?= site_url('register/'); ?>" class="text-primary m-l-5"><b>Registrate</b></a>
                            <a href="" target="_blank"><i class="fab fa-youtube-play"></i></a>
                        </div>
                    </div>
                </form>
                <form class="form-horizontal" id="recoverform">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recupera la Contraseña</h3>
                            <p class="text-muted">Ingrese su correo electrónico y se le enviarán instrucciones </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" name="email_recover" id="email_recover" type="text" required="" placeholder="Ingresa tu Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Recuperar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>

    <?php $this->load->view('template/footerLogin'); ?>
</body>

</html>