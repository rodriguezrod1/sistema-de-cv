<?php $this->load->view('template/headLogin'); ?>
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">CV Coronel Pringles</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div class="row justify-content-center m-t-15">
        <div class="col-md-8">
            <section id="wrapper" class="">
                <div class="card">
                    <div class="card-body">
                        <form class="form-horizontal form-material" id="formRegistro">
                            <!-- <div class="text-center">
                        <a href="javascript:void(0)" class="db"><img src="<?= base_url(); ?>assets/images/logo-icon.png" alt="Home" /><br /><img src="<?= base_url(); ?>assets/images/logo-text.png" alt="Home" /></a>
                    </div> -->
                            <h3 class="box-title m-t-5 m-b-0 text-center">Resgistro Coronel Pringles</h3>
                            <!--<small class="text-center">Crea tu cuenta</small>-->

                            <div class="form-group m-t-20">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input autofocus class="form-control" type="text" name="nombre" id="nombre" required="" placeholder="Nombre(s)">
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" name="apellido" id="apellido" required="" placeholder="Apellido(s)">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <input class="form-control" type="number" name="dni" id="dni" required="" placeholder="DNI">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="foto">Foto (1 MB max)</label>
                                        <input class="form-control" type="file" name="foto" id="foto" required="" placeholder="Foto (1 MB max)">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select required class="form-control" name="recide" id="recide">
                                            <option value="">¿Reside en Coronel Pringle?</option>
                                            <option value="1">Sí</option>
                                            <option value="2">No</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <select disabled class="form-control" required name="direccion_recide" id="direccion_recide">
                                            <option value="">Dirección</option>
                                            <?php foreach ($address as $value): ?>
                                                <option value="<?= $value->id ?>"><?= $value->direccion ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div  class="form-group">
                                <div class="row">
                                    <div class="col-md-3  verRequerido">
                                        <input class="form-control  requerido" type="text" name="provincia" id="provincia" required="" placeholder="Provincia">
                                    </div>
                                    <div class="col-md-3  verRequerido">
                                        <input class="form-control  requerido" type="text" name="localidad" id="localidad" required="" placeholder="Localidad">
                                    </div>
                                    <div class="col-md-3  verRequerido">
                                        <input class="form-control  requerido" type="text" name="calle" id="calle" required="" placeholder="Calle">
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" type="text" name="altura" id="altura" required placeholder="Altura">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <input class="form-control" type="number" name="piso" id="piso" placeholder="Piso Nro. (Opcional)">
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" type="number" name="departamento" id="departamento" placeholder="Departamento (Opcional)">
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" type="number" name="telefono_1" id="telefono_1" required="" placeholder="Teléfono Personal">
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" required type="number" name="telefono_2" id="telefono_2" placeholder="Teléfono Secundario">
                                    </div>
                                </div>
                            </div>


                            <div class="form-group ">
                                <div class="row">
                                    <div class="col-md-2">
                                        <select class="form-control" name="genero" required id="genero">
                                            <option value="">Genero</option>
                                            <option value="Masculino">Masculino</option>
                                            <option value="Femenino">Femenino</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control" type="email" name="correo" id="correo" required placeholder="Correo Electrónico">
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" type="password" name="password" id="password" required="" placeholder="Password">
                                    </div>
                                    <div class="col-md-3">
                                        <input class="form-control" type="password" name="password_repeat" id="password_repeat" required="" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-center m-t-20 justify-content-end">
                                <div class="row justify-content-center">
                                    <div class="col-md-4">
                                        <button id="btnProcesar" class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">
                                            Procesar
                                        </button>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group m-b-0">
                                <div class="col-sm-12 text-center">
                                    <p>¿Ya tienes cuenta? <a href="<?= site_url('/'); ?>" class="text-info m-l-5"><b>Login</b></a></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php $this->load->view('template/footerLogin'); ?>
</body>

</html>