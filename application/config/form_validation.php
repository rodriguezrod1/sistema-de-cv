<?php

defined('BASEPATH') or exit('No direct script access allowed');

$CI = &get_instance();
$CI->load->model('Users_model', 'users');



$config['register-cv'] = [
    [
        'field'  => 'formacion',
        'label'  => 'formacion',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Seleccione la formación'
        ]
    ],
    [
        'field'  => 'observaciones_f',
        'label'  => 'observaciones_f',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Titulo e Institución es requerido'
        ]
    ],
    [
        'field'  => 'conducir_id',
        'label'  => 'conducir_id',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Seleccione tipo carnet conducir'
        ]
    ],
    [
        'field'  => 'cursos',
        'label'  => 'cursos',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Cursos realizados es requerido'
        ]
    ],
    [
        'field'  => 'disponibilidad',
        'label'  => 'disponibilidad',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'la Disponibilidad es requerido'
        ]
    ],
    [
        'field'  => 'idiomas',
        'label'  => 'idiomas',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'El Idioma es requerido'
        ]
    ],
    [
        'field'  => 'word',
        'label'  => 'word',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Seleccione conocimientos Word'
        ]
    ],
    [
        'field'  => 'excel',
        'label'  => 'excel',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Seleccione conocimientos Excel'
        ]
    ],
    [
        'field'  => 'internet',
        'label'  => 'internet',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Seleccione conocimientos en Internet'
        ]
    ],
    [
        'field'  => 'compartir',
        'label'  => 'compartir',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Seleccione si/no compartir CV'
        ]
    ],
    [
        'field'  => 'trab_anterior',
        'label'  => 'trab_anterior',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Seleccione si es trab. anterior'
        ]
    ],
    [
        'field'  => 'referencias',
        'label'  => 'referencias',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Las Refrencias son requeridas'
        ]
    ]
];


$config['register-user'] = [
    [
        'field'  => 'nombre',
        'label'  => 'nombre',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'El nombre es requerido'
        ]
    ],
    [
        'field'  => 'apellido',
        'label'  => 'apellido',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'El Apellido es requerido'
        ]
    ],
    [
        'field'  => 'dni',
        'label'  => 'dni',
        'rules'  => [
            'trim',
            'required',
            'numeric',
            ['isUniqueLogin', [$CI->users, 'isUniqueLogin']]
        ],
        'errors' => [
            'required' => 'El DNI es requerido',
            'numeric'  => 'El DNI debe ser numérico',
            'isUniqueLogin' => 'El DNI ya está registrado'
        ]
    ],
    [
        'field'  => 'recide',
        'label'  => 'recide',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Seleccione si recide en Pringles'
        ]
    ],
    [
        'field'  => 'telefono_1',
        'label'  => 'telefono_1',
        'rules'  => 'trim|required|numeric',
        'errors' => [
            'required' => 'El Teléfono principal es requerido',
            'numeric'  => 'El Teléfono principal debe ser numérico'
        ]
    ],
    [
        'field'  => 'telefono_2',
        'label'  => 'telefono_2',
        'rules'  => 'trim|required|numeric',
        'errors' => [
            'required' => 'El Teléfono secundario es requerido',
            'numeric'  => 'El Teléfono secundario debe ser numérico'
        ]
    ],
    [
        'field'  => 'genero',
        'label'  => 'genero',
        'rules'  => 'trim|required',
        'errors' => [
            'required' => 'Seleccione el Genero'
        ]
    ],
    [
        'field'  => 'correo',
        'label'  => 'correo',
        'rules'  => [
            'trim',
            'required',
            'valid_email',
            ['isUniqueLogin', [$CI->users, 'isUniqueLoginEmail']]
        ],
        'errors' => [
            'required' => 'El Correo es requerido',
            'valid_email'   => 'El correo electrónico introducido no ha sido formateado correctamente',
            'isUniqueLogin' => 'El Correo ya está registrado'
        ]
    ],
    [
        'field'  => 'password',
        'label'  => 'password',
        'rules'  => 'trim|required|min_length[8]',
        'errors' => [
            'required' => 'La Clave es requerida',
            'min_length' => 'La contraseña no puede contener menos de 8 caracteres.'
        ]
    ],
    [
        'field'  => 'password_repeat',
        'label'  => 'password_repeat',
        'rules'  => 'required|matches[password]',
        'errors' => [
            'required' => 'Ingrese la confirmación de la contraseña',
            'matches'  => 'Las contraseñas deben ser las mismas'
        ]
    ]
];


$config['sign-in'] = [
    [
        'field'  => 'login',
        'label'  => 'login',
        'rules'  => 'trim|required|numeric',
        'errors' => [
            'required' => 'El Usuario/DNI es requerido',
            'numeric'  => 'El DNI debe ser numérico'
        ]
    ],
    [
        'field'  => 'password',
        'label'  => 'password',
        'rules'  => 'required',
        'errors' => [
            'required' => 'Introduce la contraseña'
        ]
    ]
];

$config['get-by-id'] = [
    [
        'field'  => 'id',
        'label'  => 'id',
        'rules'  => 'trim|required|numeric',
        'errors' => [
            'required' => 'El ID es requerido',
            'numeric'  => 'El ID debe ser numérico'
        ]
    ]
];



$config['reset-user-password'] = [
    [
        'field'  => 'email_recover',
        'label'  => 'email_recover',
        'rules'  => 'trim|required|valid_email',
        'errors' => [
            'required'    => 'correo electronico es requerido',
            'valid_email' => 'El correo electrónico introducido no ha sido formateado correctamente'
        ]
    ]
];

$config['change-password'] = [
    [
        'field'  => 'current_password',
        'label'  => 'current_password',
        'rules'  => [
            'required',
            ['isCurrentPassword', [$CI->users, 'isCurrentPassword']]
        ],
        'errors' => [
            'required'          => 'Se requiere la contraseña actual',
            'isCurrentPassword' => 'La contraseña actual no es correcta'
        ]
    ],
    [
        'field'  => 'password',
        'label'  => 'password',
        'rules'  => 'required|min_length[8]',
        'errors' => [
            'required'   => 'Se requiere la contraseña',
            'min_length' => 'La contraseña no puede contener menos de 8 caracteres.'
        ]
    ],
    [
        'field'  => 'password_repeat',
        'label'  => 'password_repeat',
        'rules'  => 'required|matches[password]',
        'errors' => [
            'required' => 'Las contraseñas deben ser las mismas',
            'matches'  => 'Las contraseñas deben ser las mismas'
        ]
    ]
];


$config['recover-password'] = [
    [
        'field'  => 'password',
        'label'  => 'password',
        'rules'  => 'required|min_length[8]',
        'errors' => [
            'required'   => 'Se requiere la contraseña',
            'min_length' => 'La contraseña no puede contener menos de 8 caracteres.'
        ]
    ],
    [
        'field'  => 'password_repeat',
        'label'  => 'password_repeat',
        'rules'  => 'required|matches[password]',
        'errors' => [
            'required' => 'Las contraseñas deben ser las mismas',
            'matches'  => 'Las contraseñas deben ser las mismas'
        ]
    ]
];
