<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('UsersModel', 'users');
        header('Content-Type: application/json');
    }

    public function setRegistroCv()
    {
        echo json_encode($this->users->setRegistroCv());
    }

    public function recoverPassword()
    {
        echo json_encode($this->users->recoverPassword());
    }

    public function signIn()
    {
        echo json_encode($this->users->signIn());
    }

    public function signOut()
    {
        echo json_encode($this->users->signOut());
    }

    public function register()
    {
        echo json_encode($this->users->register());
    }

    public function toggleStatus()
    {
        echo json_encode($this->users->toggleStatus());
    }

    public function resetPassword()
    {
        echo json_encode($this->users->resetPassword());
    }

    public function changePassword()
    {
        echo json_encode($this->users->changePassword());
    }

    public function getBySiteId()
    {
        echo json_encode($this->users->getBySiteId());
    }

    public function delete()
    {
        echo json_encode($this->users->delete());
    }

}
