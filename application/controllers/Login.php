<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        header('Content-Type: text/html');

        if (isset($this->session->id))
        {
            redirect(base_url('home'));
            exit;
        }
    }

    public function index()
    {
        $data['title'] = 'Login ';
        $this->load->view('login/login_view', $data);
    }

    public function register()
    {
        $data['title']   = 'Registro ';
        $data['address'] = $this->users->getDireccionesPringles(); 
        $this->load->view('login/register_view', $data);
    }

}
