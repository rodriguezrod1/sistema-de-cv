<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!isset($this->session->id))
        {
            redirect(base_url('login'));
            exit;
        }

        $this->load->model('UsersModel', 'users');
        header('Content-Type: text/html');
    }

    public function index()
    {
        //$data['title']    = 'Gestión de Usuarios';
        //$this->load->view('users/users_view', $data);
    }

   
    public function password()
    {
        $data['title'] = 'Cambiar Password';
        $this->load->view('users/clave_view', $data);
    }

}
