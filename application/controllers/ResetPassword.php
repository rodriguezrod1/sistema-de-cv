<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ResetPassword extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        header('Content-Type: text/html');
    }

    public function verify($token, $id)
    {
        if (!isset($token) && !isset($id)) {
            redirect(base_url());
            exit;
        }
        $data['result'] = $this->users->verifyCode($token, $id);
        $data['id']     = $id;
        $this->load->view('login/resetPassword_view', $data);
    }
}
