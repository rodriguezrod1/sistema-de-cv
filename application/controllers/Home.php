<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!isset($this->session->id)) {
            redirect(base_url('login'));
            exit;
        }
        $this->load->model('UsersModel', 'users');
        header('Content-Type: text/html');
    }

    public function index()
    {
        $data['data']    = $this->users->getDatosPersonales();
        $data['tipo']    = $this->users->getTiposCarnet();
        $data['title']   = 'Bienvenid@s: ' . $this->session->names;
        $this->load->view('home/home_view', $data);
    }

    public function pdf()
    {
        require_once APPPATH . '../vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf();
        $data['data']    = $this->users->getDatosPersonales();
        //Load html view
        $mpdf->WriteHTML($this->load->view('home/pdf_view', $data, TRUE));
        $mpdf->Output();
    }
}
