$("#form-plantillas").on('submit', function(e) {
    e.preventDefault();
    var form = $('#form-plantillas');
    var btn = $('#texto2');
    $.ajax({
        type: "POST",
        url: `${$('#base_api').val()}api/Mensajes/updateTemplate`,
        data: form.serialize(),
        beforeSend: function() {
            $('button, input, select, textarea').attr("disabled", true);
            btn.html('Procesando...');
        },
        success: function(json) {
            $('button, input, select, textarea').attr("disabled", false);
            btn.html('Guardar');
            if (NotificationService(json)) {
                form[0].reset();
                btn.html('Guardar');
            }
        }
    });
});




$("#form-agregar-tipo").on('submit', function(e) {
    e.preventDefault();
    var form = $('#form-agregar-tipo');
    var btn = $('#texto');
    $.ajax({
        type: "POST",
        url: `${$('#base_api').val()}api/Mensajes/setTipoPlantilla`,
        data: form.serialize(),
        beforeSend: function() {
            $('button, input, select').attr("disabled", true);
            btn.html('Procesando...');
        },
        success: function(json) {
            $('button, input, select').attr("disabled", false);
            btn.html('Enviar');
            if (NotificationService(json)) {
                $('#modal-agregar-articulo').modal('hide');
                form[0].reset();
                $('#table_tipo_plantilla').DataTable().ajax.reload();
                getTipoSelect();
            }
        }
    });

});

$('.textarea_editor').wysihtml5();
getTipoSelect();


//---------------------------------------------------------------------------------

$('#table_tipo_plantilla').dataTable({
    columnDefs: [{
        orderable: false,
        targets: [1]
    }],
    pageLength: 8,
    lengthMenu: [
        [5, 8, 15, 20],
        [5, 8, 15, 20]
    ],
    autoWidth: false,
    dom: 'Bfrtip',
    buttons: [{
            extend: 'pdf',
            className: 'btn btn-outline-info',
            text: 'PDF',
            exportOptions: {
                columns: [1, 2]
            }
        },
        {
            extend: 'excel',
            className: 'btn btn-outline-info',
            text: 'Excel',
            exportOptions: {
                columns: [1, 2]
            }

        },
        {
            extend: 'print',
            className: 'btn btn-outline-info',
            text: 'Imprimir',
            exportOptions: {
                columns: [1, 2]
            }
        }
    ],
    "scrollX": false,
    "processing": true,
    "responsive": true,
    "ajax": `${$('#base_api').val()}api/Mensajes/getTipoPlantilla`,
    columns: [{
            data: "tipo",
            title: 'Nombre',
            className: 'nombre1'
        },
        {
            data: "creado",
            title: 'Creado'
        },
        {
            sTitle: "Actualizado",
            mDataProp: "actualizado",
            render: function(data, type, item) {
                return data == null ? "No Actualizado" : data;
            }
        }

    ],
    "fnDrawCallback": function() {
        //Initialize checkbos for enable/disable user
        //var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch4'));
        //$('.js-switch4').each(function() {
        //  new Switchery($(this)[0], $(this).data());
        //});
    },
    "aaSorting": [
        [1, "desc"]
    ],
    rowId: 'id'
});




// Campo nombre tipo
//-------------------------------------------------------------------------------------------------------------
$(document).on('dblclick', '#table_tipo_plantilla tbody .nombre1', function(e) {
    e.defaultPrevented;
    var text = $(this).text();
    var html = '<div class="form-group"><input style="width: 200px" title="Nombre del Tipo Template" id="nombre" class="form-control" type="text" name="nombre">';
    $(this).html(html);
    $(this).removeClass('nombre1');
    $(this).addClass('nombre');
    $('#nombre').focus();
    $('#nombre').val(text);
});

$(document).on('blur', '#table_tipo_plantilla tbody .nombre', function(e) {
    var tabla = "table_tipo_plantilla";
    var nombreCampoBD = "tipo";
    var valor = $('#nombre').val();
    var uriAjax = "Market/updateAutoMarket";
    var nombreTablaBD = "tipoPlantilla";
    var idCampoActual = "nombre";
    updateData($(this).closest('tr').attr('id'), nombreCampoBD, valor, tabla, uriAjax, nombreTablaBD, idCampoActual, $(this));
    getTipoSelect();
});

// Fin de caso de actualizacion 
//    -----------------------------------------------------------------------------------------------------