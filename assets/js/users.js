$(function() {



    $('#table_user').dataTable({
        columnDefs: [{
            orderable: false,
            targets: [1]
        }],
        pageLength: 8,
        lengthMenu: [
            [5, 8, 15, 20],
            [5, 8, 15, 20]
        ],
        autoWidth: false,
        dom: 'Bfrtip',
        buttons: [{
                extend: 'pdf',
                className: 'btn btn-outline-info',
                text: 'PDF',
                exportOptions: {
                    columns: [1, 2, 3, 4]
                }
            },
            {
                extend: 'excel',
                className: 'btn btn-outline-info',
                text: 'Excel',
                exportOptions: {
                    columns: [1, 2, 3, 4]
                }

            },
            {
                extend: 'print',
                className: 'btn btn-outline-info',
                text: 'Imprimir',
                exportOptions: {
                    columns: [1, 2, 3, 4]
                }
            }
        ],
        "scrollX": false,
        "processing": true,
        "responsive": true,
        "ajax": `${$('#base_api').val()}api/Users/getUsers`,
        columns: [{
                data: "name",
                title: 'Nombre',
                className: 'nombre1'
            },
            {
                data: "tipo_id",
                title: 'Tipo',
                className: 'tipo1'
            },
            {
                data: "genero",
                title: 'Genero',
                className: 'genero1'
            },
            {
                data: "login",
                title: 'Email',
                className: 'login1'
            },
            {
                sTitle: "Teléfono",
                mDataProp: "phone",
                className: 'phone1 text-center',
                render: function(data, type, item) {
                    return data == null ? "--" : data;
                }
            },
            {
                sTitle: "Estado",
                mDataProp: "id",
                sWidth: '10%',
                orderable: false,
                render: function(data, type, item) {
                    var estado = item.is_active == 1 ? "checked" : "";
                    return ' <input id="is_active-' + data + '" value="' + item.is_active + '" name="users" type="checkbox" class="js-switch3 status_table"  ' + estado + '  data-color="#009efb" data-secondary-color="#f62d51" >' +
                        '<span class="hide" id="text-estado' + data + '">Procesando...</span>';
                }
            },
            {
                data: "created_at",
                title: 'Creado'
            },
            {
                sTitle: "Actualizado",
                mDataProp: "actualizado",
                render: function(data, type, item) {
                    return data == null ? "No Actualizado" : data;
                }
            },
            {
                sTitle: "Borrar",
                mDataProp: "id",
                className: 'text-center',
                sWidth: '5%',
                orderable: false,
                render: function(data, type, item) {
                    return "<button type='button' class='btn btn-sm btn-outline-danger alerta_delete2 ' id='users-table_user-" + data + "' ><i class='fa fa-trash'></i></button>";
                }
            }
        ],
        "fnDrawCallback": function() {
            //Initialize checkbos for enable/disable user
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch3'));
            $('.js-switch3').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
        },
        "aaSorting": [
            [1, "asc"]
        ],
        rowId: 'id'
    });



    // Campo Teléfono 
    //-------------------------------------------------------------------------------------------------------------
    $(document).on('dblclick', '#table_user tbody .phone1', function(e) {
        e.defaultPrevented;
        var text = $(this).text();
        var html = '<div class="form-group"><input style="width: 200px" title="Teléfono del Usuario" id="phone" class="form-control" type="number" name="phone">';
        $(this).html(html);
        $(this).removeClass('phone1');
        $(this).addClass('phone');
        $('#phone').focus();
        $('#phone').val(text);
    });

    $(document).on('blur', '#table_user tbody .phone', function(e) {
        var tabla = "table_user";
        var nombreCampoBD = "phone";
        var valor = $('#phone').val();
        var uriAjax = "Market/updateAutoMarket";
        var nombreTablaBD = "users";
        var idCampoActual = "phone";

        updateData($(this).closest('tr').attr('id'), nombreCampoBD, valor, tabla, uriAjax, nombreTablaBD, idCampoActual, $(this));
    });

    // Campo Nombre 
    //-------------------------------------------------------------------------------------------------------------
    $(document).on('dblclick', '#table_user tbody .nombre1', function(e) {
        e.defaultPrevented;
        var text = $(this).text();
        var html = '<div class="form-group"><input style="width: 200px" title="Nombre del Usuario" id="nombre" class="form-control" type="text" name="nombre">';
        $(this).html(html);
        $(this).removeClass('nombre1');
        $(this).addClass('nombre');
        $('#nombre').focus();
        $('#nombre').val(text);
    });

    $(document).on('blur', '#table_user tbody .nombre', function(e) {
        var tabla = "table_user";
        var nombreCampoBD = "name";
        var valor = $('#nombre').val();
        var uriAjax = "Market/updateAutoMarket";
        var nombreTablaBD = "users";
        var idCampoActual = "nombre";

        updateData($(this).closest('tr').attr('id'), nombreCampoBD, valor, tabla, uriAjax, nombreTablaBD, idCampoActual, $(this));
    });

    // Fin de caso de actualizacion 
    //    -----------------------------------------------------------------------------------------------------



    // Campo Tipo usuario
    //-------------------------------------------------------------------------------------------------------------
    $(document).on('dblclick', '#table_user tbody .tipo1', function(e) {
        e.defaultPrevented;
        var text = $.trim($(this).text());
        var html = '<div class="form-group "><select style="width: 200px" id="type" name="type" class="form-control"></select></div>';
        $(this).html(html);
        $('#tipo').find('option').clone().appendTo('#type');
        $("#type option:contains(" + text + ")").attr("selected", true);
        $(this).removeClass('tipo1');
        $(this).addClass('type');
        $('#type').focus();
    });

    $(document).on('blur', '#table_user tbody .type', function(e) {
        var tabla = "table_user";
        var nombreCampoBD = "tipo_id";
        var valor = $('#type').val();
        var uriAjax = "Market/updateAutoMarket";
        var nombreTablaBD = "users";
        var idCampoActual = "type";

        updateData($(this).closest('tr').attr('id'), nombreCampoBD, valor, tabla, uriAjax, nombreTablaBD, idCampoActual, $(this));
    });

    // Fin de caso de actualizacion del campo 
    //    ------------------------



    // Campo Tipo Genero
    //-------------------------------------------------------------------------------------------------------------
    $(document).on('dblclick', '#table_user tbody .genero1', function(e) {
        e.defaultPrevented;
        var text = $.trim($(this).text());
        var html = '<div class="form-group "><select style="width: 200px" id="genero" name="genero" class="form-control"><option value="Masculino">Masculino</option><option value="Femenino">Femenino</option></select></div>';
        $(this).html(html);
        //$('#tipo').find('option').clone().appendTo('#type');
        $("#genero option:contains(" + text + ")").attr("selected", true);
        $(this).removeClass('genero1');
        $(this).addClass('genero');
        $('#genero').focus();
    });

    $(document).on('blur', '#table_user tbody .genero', function(e) {
        var tabla = "table_user";
        var nombreCampoBD = "genero";
        var valor = $('#genero').val();
        var uriAjax = "Market/updateAutoMarket";
        var nombreTablaBD = "users";
        var idCampoActual = "genero";

        updateData($(this).closest('tr').attr('id'), nombreCampoBD, valor, tabla, uriAjax, nombreTablaBD, idCampoActual, $(this));
    });

    // Fin de caso de actualizacion del campo 
    //    ------------------------




    // Campo Nombre 
    //-------------------------------------------------------------------------------------------------------------
    $(document).on('dblclick', '#table_user tbody .login1', function(e) {
        e.defaultPrevented;
        var text = $(this).text();
        var html = '<div class="form-group"><input style="width: 200px" title="Usuario" id="login" class="form-control" type="text" name="login">';
        $(this).html(html);
        $(this).removeClass('login1');
        $(this).addClass('login');
        $('#login').focus();
        $('#login').val(text);
    });

    $(document).on('blur', '#table_user tbody .login', function(e) {
        var tabla = "table_user";
        var nombreCampoBD = "login";
        var valor = $('#login').val();
        var uriAjax = "Market/updateAutoMarket";
        var nombreTablaBD = "users";
        var idCampoActual = "login";

        updateData($(this).closest('tr').attr('id'), nombreCampoBD, valor, tabla, uriAjax, nombreTablaBD, idCampoActual, $(this));
    });

    // Fin de caso de actualizacion 
    //    -----------------------------------------------------------------------------------------------------



});