function NotificationService(obj) {
    switch (obj.code) {
        //                    Errores de Validación
        case 800:
            var msj = [];
            $.each(obj.data, function(i, v) {
                msj.push(v + "<br>");
            });
            toast(msj, 'error');
            return false;
            break;
            //                        Error de fin de sesión
        case 900:
            toast('Su Sesión a Finalizado, por favor vuelva a Ingresar.', 'warning');
            setTimeout(function() {
                location = `${$('#base_api').val()}Logout/`;
            }, 2000);
            break;
            //                        Respuesta del método que procesa error / success
        default:
            if (obj.status === 'error') {
                toast(obj.message, 'error');
                return false;
            }
            toast(obj.message, 'success');
            return true;
            break;
    }
}

function toast(mens, alert) {
    var title;
    switch (alert) {
        case 'info':
            title = '¡ Importante !';
            break;
        case 'warning':
            title = '¡ Precaución !';
            break;
        case 'success':
            title = '¡ Listo !';
            break;
        default:
            title = '¡ Disculpe !';
            break;
    }

    $.toast({
        heading: title,
        text: mens,
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: alert,
        hideAfter: 6000,
        showHideTransition: 'slide',
        stack: 6
    });
}

function formato_numero(numero, decimales, separador_decimal, separador_miles) {
    numero = parseFloat(numero);
    if (isNaN(numero)) {
        return "";
    }

    if (decimales !== undefined) {
        // Redondeamos
        numero = numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero = numero.toString().replace(".", separador_decimal !== undefined ? separador_decimal : ",");

    if (separador_miles) {
        // Añadimos los separadores de miles
        var miles = new RegExp("(-?[0-9]+)([0-9]{3})");
        while (miles.test(numero)) {
            numero = numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}


$(document).ready(function() {


    $.ajaxSetup({
        error: function(jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
            $('button, input, select, textarea').attr("disabled", false);
        }
    });



    // Registro de nuevo usuario desde el login
    $("#formRegistro").on('submit', function(e) {
        e.preventDefault();
        var form = $('#formRegistro');
        var btn = $('#btnProcesar');
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/register`,
            data: new FormData(this), //enviamos imagen
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('button, input, select, textarea').attr("disabled", true);
                btn.val('Procesando...');
            },
            success: function(json) {
                $('button, input, select, textarea').attr("disabled", false);
                btn.val('Procesar');
                if (NotificationService(json)) {
                    form[0].reset();
                    setTimeout(function() {
                        location = `${$('#base_api').val()}Home/`;
                    }, 1000);
                }
            }
        });
    });



    $('#loginform').submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/signIn`,
            data: $('#loginform').serialize(),
            beforeSend: function() {
                $('button, input').attr("disabled", true);
                $('#text_login').html('Verificando...');
            },
            success: function(json) {
                $('button, input').attr("disabled", false);
                $('#text_login').html('Ingresar');

                if (NotificationService(json)) {
                    setTimeout(function() {
                        location = `${$('#base_api').val()}Home/`;
                    }, 500);
                }
            }
        });
    });


    $('#recoverform').submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/resetPassword`,
            data: $('#recoverform').serialize(),
            beforeSend: function() {
                $('button, input').attr("disabled", true);
                $('#text_recover').html('Verificando...');
            },
            success: function(json) {
                $('button, input').attr("disabled", false);
                $('#text_recover').html('Recuperar');
                NotificationService(json);
            }
        });
    });



    $('#resetPassword').submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/recoverPassword`,
            data: $('#resetPassword').serialize(),
            beforeSend: function() {
                $('button, input').attr("disabled", true);
                $('#text_recover').html('Procesando...');
            },
            success: function(json) {
                $('button, input').attr("disabled", false);
                $('#text_recover').html('Cambiar');
                if (NotificationService(json)) {
                    setTimeout(function() {
                        location = `${$('#base_api').val()}`;
                    }, 1000);
                }
            }
        });
    });


    $(document).on('change', '.status_table', function(event, state) {
        var i = $(this).attr('id');
        var sep = i.split("-");
        var id = sep[1];
        var campo = sep[0];
        var valor = $(this).val();
        var tabla = $(this).attr('name');
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/toggleStatus`,
            data: { 'campo': campo, 'valor': valor, 'id': id, 'tabla': tabla },
            beforeSend: function() {
                $(this).addClass("hidden");
                $('#text-' + campo + id).removeClass("hidden");
            },
            success: function(data) {
                $(this).removeClass("hidden");
                $('#text-' + campo + id).addClass("hidden");
                if (NotificationService(data)) {
                    $('#' + i).val(data.value);
                    if (data.recargar == 1) {
                        $('#table_user_market').DataTable().ajax.reload();
                        $('#table_solicitudes').DataTable().ajax.reload();
                    }
                }
            }
        });
    });



    $('.status_table2').on('switchChange.bootstrapSwitch', function(event, state) {
        var i = $(this).attr('id');
        var sep = i.split("-");
        var id = sep[1];
        var campo = sep[0];
        var valor = $(this).val();
        var tabla = $(this).attr('name');
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/toggleStatus`,
            data: { 'campo': campo, 'valor': valor, 'id': id, 'tabla': tabla },
            beforeSend: function() {
                $(this).addClass("hidden");
                $('#text-' + campo + id).removeClass("hidden");
            },
            success: function(data) {
                $(this).removeClass("hidden");
                $('#text-' + campo + id).addClass("hidden");
                if (NotificationService(data)) {
                    $('#' + i).val(data.value);

                    if (data.recargar == 1) {
                        $('#table_user_market').DataTable().ajax.reload();
                    }
                }
            }
        });
    });




    $("#form_cambio_de_clave").on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/changePassword`,
            data: $('#form_cambio_de_clave').serialize(),
            beforeSend: function() {
                $('button, input, select').attr("disabled", true);
                $('#text_contact').html('Processing...');
            },
            success: function(json) {
                $('button, input, select').attr("disabled", false);
                $('#text_contact').html('Process');
                if (NotificationService(json)) {
                    $('#form_cambio_de_clave')[0].reset();
                }
            }
        });
    });


    $("#form-agregar-user").on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/register`,
            data: $('#form-agregar-user').serialize(),
            beforeSend: function() {
                $('button, input, select').attr("disabled", true);
                $('#text_contact').html('Procesando...');
            },
            success: function(json) {
                $('button, input, select').attr("disabled", false);
                $('#text_contact').html('Agregar');
                if (NotificationService(json)) {
                    $('#form-agregar-user')[0].reset();
                    $('#modal-agregar').modal('hide');
                    $('#table_user').DataTable().ajax.reload();
                }
            }
        });
    });


    $(document).on('click', '.alerta_delete2', function(e) {
        e.preventDefault();
        var i = $(this).attr('id');
        swal({
            title: "Estas segur@ de realizar esta operación?",
            text: "No podras recuperar el registro una vez borrado!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sí, Borrar!",
            cancelButtonText: "No, Cancelar!",
            html: false,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    setTimeout(function() {
                        resolve();
                    }, 50);
                });
            }
        }).then(function(result) {
            if (result.value) {
                var sep = i.split("-");
                var id = sep[2];
                var tabla = sep[0];
                var dataTable = sep[1];
                $.ajax({
                    type: "POST",
                    url: `${$('#base_api').val()}api/users/delete`,
                    data: { 'tabla': tabla, 'id': id },
                    beforeSend: function() {
                        $('#borrar' + id).removeClass("fa-trash").addClass("fa-spinner fa-spin");
                        $('#' + i).attr("disabled", true);
                    },
                    success: function(data) {
                        $('#borrar' + id).removeClass("fa-spinner fa-spin").addClass("fa-trash");
                        $('#' + i).attr("disabled", false);
                        if (NotificationService(data)) {
                            swal("Eliminado!", "Registro borrado con exito.", "success");
                            $('#' + dataTable).DataTable().ajax.reload();
                        } else {
                            swal("Disculpe !", "Hay un error procesando esta operación.)", "error");
                        }
                    }
                });
            } else if (result.dismiss === 'cancel') {
                swal("Cancelado", "Operación Cancelada :)", "error");
            }
        });
    });


    $(document).on('click', '.alerta_delete', function(e) {
        e.preventDefault();
        var i = $(this).attr('id');
        swal({
            title: "Estas segur@ de realizar esta operación?",
            text: "No podras recuperar el registro una vez borrado!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sí, Borrar!",
            cancelButtonText: "No, Cancelar!",
            html: false,
            preConfirm: function() {
                return new Promise(function(resolve) {
                    setTimeout(function() {
                        resolve();
                    }, 50);
                });
            }
        }).then(function(result) {
            if (result.value) {
                var sep = i.split("-");
                var id = sep[1];
                var tabla = sep[0];
                $.ajax({
                    type: "POST",
                    url: `${$('#base_api').val()}api/users/delete`,
                    data: { 'tabla': tabla, 'id': id },
                    beforeSend: function() {
                        $('#borrar' + id).removeClass("fa-trash").addClass("fa-spinner fa-spin");
                        $('#' + i).attr("disabled", true);
                    },
                    success: function(data) {
                        $('#borrar' + id).removeClass("fa-spinner fa-spin").addClass("fa-trash");
                        $('#' + i).attr("disabled", false);
                        if (NotificationService(data)) {
                            swal("Eliminado!", "Registro borrado con exito.", "success");
                            $('.js-dataTable-full').DataTable().row($(this).closest("tr").get(0)).remove().draw();
                        } else {
                            swal("Disculpe !", "Hay un error procesando esta operación.)", "error");
                        }
                    }
                });
            } else if (result.dismiss === 'cancel') {
                swal("Cancelado", "Operación Cancelada :)", "error");
            }
        });
    });

    $('label').addClass('form-inline');
    $(' input[type="search"]').addClass('form-control input-lg');

    $('.modal').on('shown.bs.modal', function() {
        $(this).find('[autofocus]').focus();
    });
});