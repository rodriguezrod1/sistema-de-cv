
function NotificationService(obj)
{

    switch (obj.code)
    {
//                    Errores de Validación
        case 800:
            var msj = [];
            $.each(obj.data, function (i, v)
            {
                msj.push(v);
            });
            toast(msj, 'error');
            return false;
            break;
//                        Error de fin de sesión
        case 900:
            toast('Su Sesión a Finalizado, por favor vuelva a Ingresar.', 'warning');
            setTimeout(function ()
            {
                location = `${$('#base_api').val()}Logout/`;
            }, 2000);
            break;
//                        Respuesta del método que procesa error / success
        default:
            if (obj.status === 'error')
            {
                toast(obj.message, 'error');
                return false;
            }
            toast(obj.message, 'success');
            return true;
            break;
    }
}

function toast(mens, alert)
{
    var title;
    switch (alert)
    {
        case 'info':
            title = '¡ Importante !';
            break;
        case 'warning':
            title = '¡ Precaución !';
            break;
        case 'success':
            title = '¡ Listo !';
            break;
        default:
            title = '¡ Disculpe !';
            break;
    }

    $.toast({
        heading: title,
        text: mens,
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: alert,
        hideAfter: 6000,
        showHideTransition: 'slide',
        stack: 6
    });
}


$(function() 
{
  
    $('#loginform').submit(function (e)
    {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/signIn`,
            data: $('#loginform').serialize(),
            beforeSend: function ()
            {
                $('button, input').attr("disabled", true);
                $('#text_login').html('Verificando...');
            },
            success: function (json)
            {
                $('button, input').attr("disabled", false);
                $('#text_login').html('Ingresar');

                if (NotificationService(json))
                {
                    setTimeout(function ()
                    {
                        location = `${$('#base_api').val()}Home/`;
                    }, 500);
                }
            }
        });
    });


    $('#recoverform').submit(function (e)
    {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/resetPassword`,
            data: $('#recoverform').serialize(),
            beforeSend: function ()
            {
                $('button, input').attr("disabled", true);
                $('#text_recover').html('Verificando...');
            },
            success: function (json)
            {
                $('button, input').attr("disabled", false);
                $('#text_recover').html('Recuperar');
                NotificationService(json);
            }
        });
    });



    $('#resetPassword').submit(function (e)
    {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/recoverPassword`,
            data: $('#recoverform').serialize(),
            beforeSend: function ()
            {
                $('button, input').attr("disabled", true);
                $('#text_recover').html('Verificando...');
            },
            success: function (json)
            {
                $('button, input').attr("disabled", false);
                $('#text_recover').html('Recuperar');
                NotificationService(json);
            }
        });
    });

});
