$(function() {

    $("#formResumen").on('submit', function(e) {
        e.preventDefault();
        var form = $('#formResumen');
        var btn = $('#text');
        $.ajax({
            type: "POST",
            url: `${$('#base_api').val()}api/Users/setRegistroCv`,
            data: form.serialize(),
            beforeSend: function() {
                $('button, input, select').attr("disabled", true);
                btn.html('Procesando...');
            },
            success: function(json) {
                $('button, input, select').attr("disabled", false);
                btn.html('Procesar');
                if (NotificationService(json)) {
                    //form[0].reset();
                }
            }
        });
    });


    $("#recide").change(function(e) {
        e.preventDefault();
        var valor = $(this).val();

        switch (valor) {
            case "1":
                $("#direccion_recide").attr("disabled", false).val("");
                $(".verRequerido").addClass("hide");
                $(".requerido").attr('required', false).val("");
                break;
            case "2":
                $("#direccion_recide").attr("disabled", true).val("");
                $(".verRequerido").removeClass("hide");
                $(".requerido").attr('required', true).val("");
                break;
            default:
                $("#direccion_recide").attr("disabled", true).val("");
                $(".requerido").attr('required', true).val("");
                $(".verRequerido").addClass("hide");
                break;
        }
    });

});